class RPNCalculator

  def initialize
      @calculator=[]
      @result=0
      @token=[]
  end

  def push(int)
    @calculator.push(int)
  end

  def plus
    if @calculator.length >1
        temp=@calculator.pop(2)
        @calculator.push(temp[0]+temp[1])
    else
        raise "calculator is empty"
    end

  end

  def minus
    if @calculator.length >1
         temp=@calculator.pop(2)
         @calculator.push(temp[0]-temp[1])
    else
         raise "calculator is empty"
    end
  end

  def times
    if @calculator.length >1
         temp=@calculator.pop(2)
         @calculator.push(temp[0]*temp[1])
    else
        raise "calculator is empty"
    end
  end

  def divide
    if @calculator.length >1
         temp=@calculator.pop(2)
         @calculator.push(temp[0].to_f/temp[1].to_f)
    else
        raise "calculator is empty"
    end
  end

  def value
      @calculator.last
  end

  def tokens(string)
    ops=["+", "-", "/","*"]
    @token=string.split(" ").map do |str|
      if ops.include?(str)
        str.to_sym
      else
        str.to_i
      end
    end
    @token
  end

  def evaluate(string)
    @calculator=[]
    temptoken=tokens(string)
    temptoken.each do |int|
      if int.is_a? Integer
        @calculator << int
      else
        case
        when int== :+
          @calculator=self.plus
        when int== :-
          @calculator=self.minus
        when int== :*
          @calculator=self.times
        when int== :/
          @calculator=self.divide
        else
          raise "Error"
        end
      end
    end

  @calculator[0].to_f
  end

end
